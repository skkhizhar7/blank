resource "aws_route_table" "public_rt" {
  vpc_id = var.cpv_vpc
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.cpv_igw
  }
  tags = {
    Name = var.public_rt
  }
}

resource "aws_route_table_association" "public_route_table" {
  count            = length(var.pub_subnet_id)
  subnet_id        = var.pub_subnet_id[count.index]
  route_table_id   = aws_route_table.public_rt.id
}

resource "aws_route_table" "private_route_table" {
  vpc_id = var.cpv_vpc

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.nat_id
  }

  tags = {
    Name = var.private_route_table_tag
  }
}

resource "aws_route_table_association" "private_route_table" {
    count = length(var.pri_subnet_id)
  subnet_id      = var.pri_subnet_id[count.index]
  route_table_id = aws_route_table.private_route_table.id
}

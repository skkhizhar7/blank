output "public_route_table" {
    value = aws_route_table.public_rt.id
}
output "private_route_table" {
    value = aws_route_table.private_route_table.id
}

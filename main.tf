module "vpc" {
  source           = "./vpc"
  cidr_block       = var.cidr_block
  instance_tenancy = var.instance_tenancy
  vpc_tags             = var.vpc_tags
}

module "subnet" {
  source       = "./subnet"
  subnet_cidr    = var.subnet_cidr
  cpv_vpc        = module.vpc.vpc_id
  owner          = var.owner
  environment    = var.environment
  createdby      = var.createdby
  management     = var.management
  subnet_name    = var.subnet_name
  project        = var.project
  private_subnet_name = var.private_subnet_name
  private_subnet_cidr = var.private_subnet_cidr
  azs            = var.azs
}

module "internet_gateway" {
  source     = "./igw"
  igw_name        = var.igw_name
  cpv_vpc = module.vpc.vpc_id
}

module "route_table" {
  source                  = "./route"
  cpv_igw                 = module.internet_gateway.aws_internet_gateway
  cpv_vpc                 = module.vpc.vpc_id
  public_rt               = var.public_rt
  pub_subnet_id           = module.subnet.pub_subnet_ids
  nat_id                  = module.nat.nat_id
  private_route_table_tag = var.private_route_table_tag
  pri_subnet_id           = module.subnet.pri_subnet_ids

}

module "nat" {
  source    = "./nat"
  eip_tag   = var.eip_tag
  pub_subnet = module.subnet.pub_subnet_ids
  nat_tag   = var.nat_tag
}

module "nacl" {
  source          = "./nacl"
  cpv_vpc          = module.vpc.vpc_id
  protocol        = var.protocol
  rule_no         = var.rule_no
  action          = var.action
  nacl_cidr_block = var.nacl_cidr_block
  from_port       = var.from_port
  to_port         = var.to_port
  nacl_tag        = var.nacl_tag
  pri_subnet_ids       = module.subnet.pri_subnet_ids
}

module "sg" {
  source        = "./sg"
  cpv_vpc    = module.vpc.vpc_id
  sg_tag        = var.sg_tag
  sg_cidr_block = var.sg_cidr_block
  lb_sg_name = var.lb_sg_name
  app_port = var.app_port
}

module "alb" {
  source                 = "./loadbalancer"
  lb_name                = var.lb_name
  pub_subnet_ids         = module.subnet.pub_subnet_ids
  lb_target_name         = var.lb_target_name
  lb_target_port         = var.lb_target_port
  cpv_vpc                = module.vpc.vpc_id
  health_check_path      = var.health_check_path
  app_port              = var.app_port
  lb_sg                 = module.sg.lb_sg
}

module "iam" {
  source = "./iam"

  ecs_task_execution_role = var.ecs_task_execution_role
  ecs_task_role           = var.ecs_task_role
}

module "ecs_fargate" {
  source                     = "./ecs-farget"
  ecs_cluster                = var.ecs_cluster
  ecs_task                   = var.ecs_task
  ecs_service                = var.ecs_service
  fargate_cpu                = var.fargate_cpu
  fargate_memory             = var.fargate_memory
  app_image                  = var.app_image
  app_port                   = var.app_port
  app_count                  = var.app_count
  container_name             = var.container_name
  pri_subnet_id              = module.subnet.pri_subnet_ids
  alb_target_group_arn       = module.alb.alb_target_group_arn
  aws_region                 = var.aws_region
  log_group                  = var.log_group
  ecs_security_groups = module.sg.security_group
  ecs_task_execution_role = module.iam.ecs_task_execution_role_arn
  alb_lb_arn = module.alb.alb_arn
}
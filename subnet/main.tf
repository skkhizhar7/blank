data "aws_availability_zones" "available" {
}

resource "aws_subnet" "pub_subnet" {
  count                   = length(var.subnet_name)  # Adjust to use var.subnet_name
  vpc_id                  = var.cpv_vpc
  cidr_block              = var.subnet_cidr[count.index]
  availability_zone        = var.azs[count.index % length(var.azs)]
  map_public_ip_on_launch = true
  
  tags = merge(
    tomap({"Name" = lower(join("-", [var.project, var.environment, var.subnet_name[count.index]]))}),
    local.tags,
  )
}

resource "aws_subnet" "pri_subnet" {
  count                   = length(var.private_subnet_name)  # Adjust to use var.private_subnet_name
  vpc_id                  = var.cpv_vpc
  cidr_block              = var.private_subnet_cidr[count.index]
  availability_zone        = var.azs[count.index % length(var.azs)]
  map_public_ip_on_launch = false
  
  tags = merge(
    tomap({"Name" = lower(join("-", [var.project, var.environment, var.private_subnet_name[count.index]]))}),
    local.tags,
  )
}

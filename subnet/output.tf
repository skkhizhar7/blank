output "pub_subnet_ids" {
  value = [for subnet in aws_subnet.pub_subnet : subnet.id]
}

output "pri_subnet_ids" {
  value = [for subnet in aws_subnet.pri_subnet : subnet.id]
}

output "alb_arn" {
  description = "ARN of the load balancer"
  value       = aws_alb.cpv_lb.arn
}

output "alb_dns_name" {
  description = "DNS name of the load balancer"
  value       = aws_alb.cpv_lb.dns_name
}

output "alb_target_group_arn" {
  description = "ARN of the target group"
  value       = aws_alb_target_group.cpv_lg_target.arn
}


# alb.tf

resource "aws_alb" "cpv_lb" {
  name            = var.lb_name
  subnets         = var.pub_subnet_ids
  security_groups =  var.lb_sg
}

resource "aws_alb_target_group" "cpv_lg_target" {
  name        = var.lb_target_name
  port        = var.lb_target_port
  protocol    = "HTTP"
  vpc_id      = var.cpv_vpc
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = var.health_check_path
    unhealthy_threshold = "2"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.cpv_lb.id
  port              = var.app_port
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.cpv_lg_target.id
    type             = "forward"
  }
}
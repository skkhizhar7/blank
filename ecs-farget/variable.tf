#-------------------

variable "app_image" {
  
}
variable "app_port" {
  
}
variable "fargate_cpu" {
  
}
variable "fargate_memory" {
  
}
variable "aws_region" {
  
}
variable "app_count" {
  
}
variable "ecs_cluster" {
  
}
variable "ecs_service" {
  
}
variable "ecs_task" {
  
}
variable "container_name" {
  
}
variable "pri_subnet_id" {
  
}
variable "alb_target_group_arn" {
  
}
variable "alb_lb_arn" {
  
}
variable "log_group" {
  
}

variable "container_environment" {
  description = "Environment variables for the ECS container"
  type        = list(object({
    name  = string
    value = string
  }))
  default = []
}
variable "ecs_security_groups" {
  
}

variable "ecs_task_execution_role" {
  
}
resource "aws_ecs_cluster" "main" {
  name = var.ecs_cluster
}

resource "aws_ecs_task_definition" "app" {
  family                   = var.ecs_task
  execution_role_arn       = var.ecs_task_execution_role
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  
  container_definitions = <<DEFINITION
[
  {
    "name": "${var.container_name}",
    "image": "${var.app_image}",
    "essential": true,
    "portMappings": [
      {
        "containerPort": ${var.app_port},
        "hostPort": ${var.app_port},
        "protocol": "tcp"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${var.log_group}",
        "awslogs-region": "${var.aws_region}",
        "awslogs-stream-prefix": "ecs"
      }
    },
    "environment": ${jsonencode(var.container_environment)}
  }
]
DEFINITION
}

resource "aws_ecs_service" "main" {
  name            = var.ecs_service
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = var.ecs_security_groups
    subnets          = var.pri_subnet_id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.alb_target_group_arn
    container_name   = var.container_name
    container_port   = var.app_port
  }

  depends_on = [aws_alb_listener.front_end]
}

resource "aws_alb_listener" "front_end" {
  load_balancer_arn =var.alb_lb_arn
  port              = 80
  protocol          = "HTTP"
  
  default_action {
    type = "forward"
    target_group_arn = var.alb_target_group_arn
  }
}

variable "region" {
  description = "AWS region for deployment"
  type        = string
  default     = "us-west-2"
}

variable "existing_lb_dns" {
  description = "The DNS name of the existing Application Load Balancer"
  type        = string
}

variable "api_name" {
  description = "The name of the API Gateway"
  type        = string
  default     = "APIGateway"
}

variable "path_part" {
  description = "The path part for the API resource"
  type        = string
  default     = "resource"
}

variable "stage_name" {
  description = "The deployment stage name"
  type        = string
  default     = "prod"
}


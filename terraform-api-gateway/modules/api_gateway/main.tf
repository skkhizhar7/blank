# modules/api_gateway/main.tf
resource "aws_api_gateway_rest_api" "api_gateway" {
  name        = var.api_name
  description = var.api_description
}

resource "aws_api_gateway_resource" "resource" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_rest_api.api_gateway.root_resource_id
  path_part   = var.path_part
}

resource "aws_api_gateway_method" "method" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  resource_id = aws_api_gateway_resource.resource.id
  http_method = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "alb_integration" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  resource_id = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method

  integration_http_method = "GET"
  type                    = "HTTP_PROXY"
  uri                     = var.lb_uri
}

resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [aws_api_gateway_integration.alb_integration]
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  stage_name  = var.stage_name
}

#output "api_gateway_url" {
#  value = "${aws_api_gateway_deployment.deployment.invoke_url}/${var.path_part}"
#}


variable "api_name" {
  description = "The name of the API Gateway"
  type        = string
}

variable "api_description" {
  description = "Description of the API Gateway"
  type        = string
  default     = "API Gateway routing to a load balancer"
}

variable "path_part" {
  description = "Path part for the API resource"
  type        = string
  default     = "resource"
}

variable "lb_uri" {
  description = "The URI of the existing Load Balancer"
  type        = string
}

variable "stage_name" {
  description = "The deployment stage name"
  type        = string
  default     = "prod"
}


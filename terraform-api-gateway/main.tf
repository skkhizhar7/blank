provider "aws" {
  region = var.region
}

# Call the API Gateway module
module "api_gateway" {
  source         = "./modules/api_gateway"
  api_name       = var.api_name
  path_part      = var.path_part
  lb_uri         = "http://${var.existing_lb_dns}/resource"  # Use your existing LB DNS
  stage_name     = var.stage_name
}

output "api_gateway_url" {
  value = module.api_gateway.api_gateway_url
}


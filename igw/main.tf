resource "aws_internet_gateway" "cpv_igw" {
  vpc_id = var.cpv_vpc
  tags = {
    Name = var.igw_name
  }
}
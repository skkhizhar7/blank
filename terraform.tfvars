aws_region          = "us-west-2"

#vpc
cidr_block       = "10.0.0.0/21"
instance_tenancy = "default"
vpc_tags             = "cpv_vpc"

#subnet
subnet_name = ["cpv-pub", "cpv-pub2"]
subnet_cidr = ["10.0.0.0/24", "10.0.1.0/24"]
private_subnet_name = ["cpv-pri", "cpv-pri2"]
private_subnet_cidr = ["10.0.2.0/24", "10.0.3.0/24"]
cpv_vpc   = "aws_vpc.main.id"
management  = "aventior"
project     = "cpv"
environment = "prod"
createdby   = "khizhar"
owner       = "cpv"
azs         = ["us-west-2a", "us-west-2b"]

#igw
igw_name = "cpv-igw"

#route
public_rt = "cpv-public_route"

#nat
eip_tag = "cpv_eip"
nat_tag = "cpv_nat"
private_route_table_tag = "cpv_private_routable"

#nacl
#---------------nacl------------------------#
nacl_cidr_block = "0.0.0.0/0"
nacl_tag        = "cpv-nacl"
protocol        = "tcp"
rule_no         = 100
action          = "allow"
from_port       = 0
to_port         = 0

#sg
sg_tag        = ["cpv_sg"]
sg_cidr_block = ["10.0.1.0/24"]
lb_sg_name = "cpv_lb_sg"
app_port    = 80

#loadbalancer
lb_name = "cpvloadbalancer"
lb_target_name = "cpvloadbalancertarget1"
lb_target_port = 80
health_check_path = "/"

#iam roles

ecs_task_role = "ecs_task_role"
ecs_task_execution_role = "ecs_task_execution_role"

#ecs
app_image           = "nginx:latest"
fargate_cpu         = "256"                # CPU units (e.g., 256, 512, 1024)
fargate_memory      = "512"                # Memory in MiB (e.g., 512, 1024, 2048)
app_count           = 1                   # Desired number of ECS task instances
ecs_cluster         = "cpv-ecs-cluster"
ecs_service         = "cpv-ecs-service"
ecs_task            = "cpv-ecs-task"
container_name      = "cpv-container"
log_group           = "cpv-logs"



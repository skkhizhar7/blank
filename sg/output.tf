output "security_group" {
    value = aws_security_group.sg[*].id
}

output "lb_sg" {
  description = "ID of the load balancer security group"
  value       = [aws_security_group.lb_sg.id]
}

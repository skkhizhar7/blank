resource "aws_security_group" "sg" {
  count = length(var.sg_tag)
  vpc_id      = var.cpv_vpc

  dynamic "ingress" {
    for_each = local.inbound_ports
    content {
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks       = var.sg_cidr_block
  }
}

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks       = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.sg_tag[count.index]
  }
}

locals {
 inbound_ports = [22, 15672 , 80]
}

#lb_sg

resource "aws_security_group" "lb_sg" {
  name        = var.lb_sg_name
  description = "controls access to the ALB"
  vpc_id      = var.cpv_vpc

  ingress {
    protocol    = "tcp"
    from_port   = var.app_port
    to_port     = var.app_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = var.lb_sg_name
  }
}


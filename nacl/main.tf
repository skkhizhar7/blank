resource "aws_network_acl" "nacl" {
  vpc_id = var.cpv_vpc

  egress {
    protocol   = var.protocol
    rule_no    = var.rule_no
    action     = var.action
    cidr_block = var.nacl_cidr_block
    from_port  = var.from_port
    to_port    = var.to_port
  }

  ingress {
    protocol   = var.protocol
    rule_no    = var.rule_no
    action     = var.action
    cidr_block = var.nacl_cidr_block
    from_port  = var.from_port
    to_port    = var.to_port
  }

  tags = {
    Name = var.nacl_tag
  }
}

resource "aws_network_acl_association" "nacl" {
  count          = length(var.pri_subnet_ids)
  network_acl_id = aws_network_acl.nacl.id
  subnet_id      = var.pri_subnet_ids[count.index]
  }
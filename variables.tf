variable "aws_region" {
  
}
#vpc
variable "cidr_block" { 
}
variable "instance_tenancy" { 
}
variable "vpc_tags" {
  
}

#subnet
variable "subnet_name" {
  
}

variable "subnet_cidr" {
  
}
variable "private_subnet_name" {
  
}
variable "private_subnet_cidr" {
  
}
variable "project" {
  
}
variable "environment" {
  
}
variable "owner" {
  
}
variable "management" {
  
}
variable "createdby" {
  
}
variable "azs" {

}

#igw
variable "cpv_vpc" {
  
}
variable "igw_name" {
  
}

#rout

variable "public_rt" {
  
}

#nat
variable "eip_tag" {
  
}

variable "nat_tag" {
  
}
variable "private_route_table_tag" {
  
}

#nacl
variable "protocol" {
  
}

variable "rule_no" {
  
}

variable "action" {
  
}

variable "nacl_cidr_block" {
  
}

variable "from_port" {
  
}

variable "to_port" {
  
}

variable "nacl_tag" {
  
}

#security group
variable "sg_tag" {
  
}

variable "sg_cidr_block" {
  
}

variable "app_port" {
  
}
variable "lb_sg_name" {
  
}

#loadbalancer
variable "lb_name" {
  
}

variable "lb_target_name" {
  
}

variable "lb_target_port" {
  
}

variable "health_check_path" {
  
}

#iam roles
variable "ecs_task_role" {
  
}
 variable "ecs_task_execution_role" {
   
 }

#ecs
variable "app_image" {

}


variable "fargate_cpu" {

}

variable "fargate_memory" {

}

variable "app_count" {

}

variable "ecs_cluster" {

}

variable "ecs_service" {

}

variable "ecs_task" {
}

variable "container_name" {

}
variable "container_environment" {
  description = "Environment variables for the ECS container"
  type        = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "log_group" {
  
}